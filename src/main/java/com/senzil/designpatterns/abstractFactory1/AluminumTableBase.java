/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class AluminumTableBase implements TableBase {

	private Long length;
	private Long width;
	private String color;
	private boolean foldable;

	@Override
	public String getColor() {
		return this.color;
	}

	@Override
	public boolean isFoldable() {
		return this.foldable;
	}

	@Override
	public Long getLength() {
		return this.length;
	}

	@Override
	public Long getWidth() {
		return this.width;
	}
	
	@Override
	public String toString() {
		return "Aluminum Table Base";
	}
}

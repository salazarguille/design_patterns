/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class MicrosoftWidgetFactory implements WidgetFactory {

	public Window createWindow() {
		return new MicrosoftWindow();
	}

	public Button createButton(){
		return new MicrosoftButton();
	}
}

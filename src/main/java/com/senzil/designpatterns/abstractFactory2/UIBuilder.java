/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class UIBuilder {
	
	private Platform platform;
	
	public UIBuilder(Platform platform) {
		super();
		this.platform = platform;
	}

	public Window createWidget() {
		WidgetFactory widgetFactory = this.platform.getWidgetFactory();
		Button button = widgetFactory.createButton();
		Window window = widgetFactory.createWindow();
		window.setButton(button);
		return window;
	}
}

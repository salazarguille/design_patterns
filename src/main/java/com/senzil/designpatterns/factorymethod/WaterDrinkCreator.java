/**
 * 
 */
package com.senzil.designpatterns.factorymethod;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class WaterDrinkCreator implements DrinkCreator{

	public Drink createDrink() {
		return new Water();
	}
}

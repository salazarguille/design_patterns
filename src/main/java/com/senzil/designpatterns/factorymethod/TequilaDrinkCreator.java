/**
 * 
 */
package com.senzil.designpatterns.factorymethod;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class TequilaDrinkCreator implements DrinkCreator{

	public Drink createDrink() {
		return new Tequila();
	}
}

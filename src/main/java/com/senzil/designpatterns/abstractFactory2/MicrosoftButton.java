/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class MicrosoftButton implements Button {

	private String title;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public void paint() {
		System.out.println("Painting Microsoft Button.");
	}
	
}

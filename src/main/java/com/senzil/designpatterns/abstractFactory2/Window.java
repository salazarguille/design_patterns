/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public interface Window {

	public void setTitle(String title);
	
	public void setButton(Button button);
	
	public void paint();
}

/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class WoodTableLeg implements TableLeg {

	private Long height;
	private String color;

	@Override
	public String getColor() {
		return this.color;
	}


	@Override
	public Long getHeight() {
		return this.height;
	}

	@Override
	public boolean isFoldable() {
		return false;
	}
	
	@Override
	public String toString() {
		return "Wood Table Leg";
	}

}

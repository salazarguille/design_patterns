package com.senzil.designpatterns.factorymethod;

/**
 *	TODO Add comments.
 *
 *	@author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 */
public class FactoryMethod1App {
	
	public static void main(String[] args) {
		DrinkCreator drinkCreator = new TequilaDrinkCreator();
		Drink drink = drinkCreator.createDrink();
		drink.drink();
	}
}

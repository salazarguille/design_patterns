/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public interface TableBase {

	public Long getLength();
	
	public Long getWidth();
	
	public String getColor();
	
	public boolean isFoldable();
}

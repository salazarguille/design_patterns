/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class LinuxWindow implements Window {

	private String title;
	private Button button;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public void paint() {
		System.out.println("Painting Linux Window.");
		this.button.paint();
	}

	@Override
	public void setButton(Button button) {
		this.button = button;
	}
	
}

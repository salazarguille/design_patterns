/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class MacOSWidgetFactory implements WidgetFactory {

	public Window createWindow() {
		return new MacOsWindow();
	}

	public Button createButton(){
		return new MacOsButton();
	}
}

/**
 * 
 */
package com.senzil.designpatterns.factorymethod;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public abstract class Drink {

	public abstract Double getAlcoholContent();
	
	public abstract Double getWaterContent();
	
	public abstract Double getGasContent();

	public void drink() {
		StringBuilder builder = new StringBuilder();
		builder.append("Drink: " + this.getClass().getSimpleName() + " -> ");
		builder.append("Alcohol: [" + this.getAlcoholContent() + "] ");
		builder.append("Water: [" + this.getWaterContent() + "] ");
		builder.append("Gas: [" + this.getGasContent() + "].");
		System.out.println(builder.toString());
	}
}

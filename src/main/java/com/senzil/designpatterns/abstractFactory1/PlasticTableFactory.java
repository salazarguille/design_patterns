/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class PlasticTableFactory implements TableFactory {

	@Override
	public TableLeg createTableLeg() {
		return new PlasticTableLeg();
	}

	@Override
	public TableBase createTableBase() {
		return new PlasticTableBase();
	}
}

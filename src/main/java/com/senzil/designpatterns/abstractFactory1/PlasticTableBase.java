/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class PlasticTableBase implements TableBase {

	private Long length;
	private Long width;
	private String color;

	@Override
	public String getColor() {
		return this.color;
	}

	@Override
	public boolean isFoldable() {
		return false;
	}

	@Override
	public Long getLength() {
		return this.length;
	}

	@Override
	public Long getWidth() {
		return this.width;
	}
	
	@Override
	public String toString() {
		return "Plastic Table Base";
	}
}

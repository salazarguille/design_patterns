package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 *
 *	@author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 */
public class AbstractFactory2App {
	
	public static void main(String[] args) {
		UIBuilder uiBuilder = new UIBuilder(Platform.MICROSOFT);
		Window window = uiBuilder.createWidget();
		window.paint();
	}
}

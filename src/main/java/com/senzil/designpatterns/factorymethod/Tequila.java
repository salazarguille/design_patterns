/**
 * 
 */
package com.senzil.designpatterns.factorymethod;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class Tequila extends Drink {

	@Override
	public Double getAlcoholContent() {
		return 38D;
	}

	@Override
	public Double getWaterContent() {
		return 62D;
	}

	@Override
	public Double getGasContent() {
		return 0D;
	}

}

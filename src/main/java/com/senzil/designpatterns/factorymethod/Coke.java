/**
 * 
 */
package com.senzil.designpatterns.factorymethod;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class Coke extends Drink {

	@Override
	public Double getAlcoholContent() {
		return 0D;
	}

	@Override
	public Double getWaterContent() {
		return 25D;
	}

	@Override
	public Double getGasContent() {
		return 75D;
	}

}

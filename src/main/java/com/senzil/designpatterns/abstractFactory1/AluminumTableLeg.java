/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class AluminumTableLeg implements TableLeg {

	private Long height;
	private String color;
	private boolean folding;

	@Override
	public String getColor() {
		return this.color;
	}

	@Override
	public Long getHeight() {
		return this.height;
	}

	@Override
	public boolean isFoldable() {
		return this.folding;
	}
	
	@Override
	public String toString() {
		return "Aluminum Table Leg";
	}
}

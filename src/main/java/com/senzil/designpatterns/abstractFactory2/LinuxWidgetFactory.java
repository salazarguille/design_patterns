/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class LinuxWidgetFactory implements WidgetFactory {

	public Window createWindow() {
		return new LinuxWindow();
	}

	public Button createButton(){
		return new LinuxButton();
	}
}

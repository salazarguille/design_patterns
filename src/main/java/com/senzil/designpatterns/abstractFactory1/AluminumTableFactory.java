/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class AluminumTableFactory implements TableFactory {

	@Override
	public TableLeg createTableLeg() {
		return new AluminumTableLeg();
	}

	@Override
	public TableBase createTableBase() {
		return new AluminumTableBase();
	}
}

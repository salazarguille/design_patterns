/**
 * 
 */
package com.senzil.designpatterns.abstractFactory2;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public enum Platform {

	MICROSOFT {
		@Override
		public WidgetFactory getWidgetFactory() {
			return new MicrosoftWidgetFactory();
		}
	},
	LINUX {
		@Override
		public WidgetFactory getWidgetFactory() {
			return new LinuxWidgetFactory();
		}
	},
	MAC_OS {
		@Override
		public WidgetFactory getWidgetFactory() {
			return new MacOSWidgetFactory();
		}
	}
	;
	
	public abstract WidgetFactory getWidgetFactory();
	
	@Override
	public String toString() {
		return "Platform: " + this;
	}
}

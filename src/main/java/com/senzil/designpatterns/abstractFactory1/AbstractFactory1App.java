package com.senzil.designpatterns.abstractFactory1;

/**
 *	TODO Add comments.
 *
 *	@author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 */
public class AbstractFactory1App {
	
	public static void main(String[] args) {
		TableFactory tableFactory = new PlasticTableFactory();
		TableBase tableBase = tableFactory.createTableBase();
		TableLeg tableLeg = tableFactory.createTableLeg();
		
		System.out.println(tableBase);
		System.out.println(tableLeg);
	}
}

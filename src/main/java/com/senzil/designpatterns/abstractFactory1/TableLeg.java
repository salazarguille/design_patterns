/**
 * 
 */
package com.senzil.designpatterns.abstractFactory1;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public interface TableLeg {

	public Long getHeight();
	
	public String getColor();
	
	public boolean isFoldable();
}

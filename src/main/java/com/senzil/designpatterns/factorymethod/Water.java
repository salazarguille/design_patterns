/**
 * 
 */
package com.senzil.designpatterns.factorymethod;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class Water extends Drink {

	@Override
	public Double getAlcoholContent() {
		return 0D;
	}

	@Override
	public Double getWaterContent() {
		return 100D;
	}

	@Override
	public Double getGasContent() {
		return 0D;
	}

}
